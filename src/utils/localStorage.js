export const LOCAL_STORAGE_KEYS = {
  DICTIONARY: 'dictionary',
};

export const getLocalStorageItem = (key) => localStorage.getItem(key);

export const setLocalStorageItem = (key, data) => localStorage.setItem(key, JSON.stringify(data));
