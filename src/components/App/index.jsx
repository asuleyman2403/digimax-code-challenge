import React from 'react';

import { useDictionary } from 'hooks/useDictionary';

import Search from 'components/Search';
import Actions from 'components/Actions';
import Toast from 'components/Toast';

import './styles.scss';

const App = () => {
  const {
    size,
    searchKey,
    found,
    emittedMessage,
    handleChange,
    addToDictionary,
    deleteFromDictionary,
  } = useDictionary({ messageDuration: 2000 });

  return (
    <div className="App" data-testid="app">
      <div className="App__content">
        <h1 className="App__title">DigiMax code challenge</h1>
        <Search searchKey={searchKey} handleChange={handleChange} />
        <Actions
          isLoaded={searchKey.length > 0}
          found={found}
          addToDictionary={addToDictionary}
          deleteFromDictionary={deleteFromDictionary}
        />
        <p className="App__words-count">Overall words: {size}</p>
      </div>
      <Toast message={emittedMessage} />
    </div>
  );
};

export default App;
