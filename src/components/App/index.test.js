import React from 'react';
import { act, screen, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { shallow, mount } from 'enzyme';
import { findByTestAtrr } from 'utils/test';
import App from '.';

const dictionary = `A
a
aa
aal
aalii
aam
Aani
aardvark
aardwolf
Aaron
Aaronic
Aaronical
Aaronite
Aaronitic
Aaru
Ab
aba
Ababdeh
Ababua
abac`;

const fakeFetch = () => {
  return Promise.resolve({
    text: () => Promise.resolve(dictionary),
  });
};

describe('App Tests', () => {
  let wrapper;

  // Initial values check

  it('should render without errors', () => {
    wrapper = shallow(<App />).at(0);
    const component = findByTestAtrr(wrapper, 'app');
    expect(component.length).toBe(1);
  });

  it('should load dictionary', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);

    await act(async () => {
      wrapper = mount(<App />).at(0);
    });

    const component = findByTestAtrr(wrapper, 'app');
    expect(component.text().includes(`Overall words: ${dictionary.split('\n').length}`)).toBe(true);

    global.fetch.mockClear();
  });

  it('should contain input element', async () => {
    expect(wrapper.exists('input[data-testid="search-input"]')).toBe(true);
    global.fetch.mockClear();
  });

  it('should contain invisible actions block', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);
    await act(async () => {
      wrapper = mount(<App />).at(0);
    });
    expect(wrapper.find('[data-testid="actions"]').hasClass('--loaded')).toBe(false);
    global.fetch.mockClear();
  });

  // Check dictionary search
  it('should find the word in dictionary', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);
    await act(async () => {
      wrapper = mount(<App />).at(0);
    });
    const input = findByTestAtrr(wrapper, 'search-input');
    input.simulate('change', { target: { value: 'a' } });
    const actionsBlock = findByTestAtrr(wrapper, 'actions');
    expect(actionsBlock.text().includes('The word exists in dictionary.')).toBe(true);

    global.fetch.mockClear();
  });

  it('should not find the word in dictionary', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);
    await act(async () => {
      wrapper = mount(<App />).at(0);
    });
    const input = findByTestAtrr(wrapper, 'search-input');
    input.simulate('change', { target: { value: 'b' } });
    const actionsBlock = findByTestAtrr(wrapper, 'actions');
    expect(actionsBlock.text().includes('The word exists in dictionary.')).toBe(false);

    global.fetch.mockClear();
  });

  // Dictionary manipulation check
  it('should add the word to dictionary', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);
    await act(async () => {
      wrapper = mount(<App />).at(0);
    });
    const input = findByTestAtrr(wrapper, 'search-input');
    input.simulate('change', { target: { value: 'b' } });
    const addBtn = findByTestAtrr(wrapper, 'actions-add-btn');
    addBtn.simulate('click');
    const app = findByTestAtrr(wrapper, 'app');
    expect(app.text().includes(`Overall words: ${dictionary.split('\n').length + 1}`)).toBe(true);
    global.fetch.mockClear();
  });

  it('should remove the word from dictionary', async () => {
    jest.spyOn(global, 'fetch').mockImplementation(fakeFetch);
    await act(async () => {
      wrapper = render(<App />);
    });

    // used test lib screen, user event since issue on enzyme-adapter found

    userEvent.type(screen.getByTestId('search-input'), 'a');
    userEvent.click(screen.getByTestId('actions-delete-btn'));
    expect(screen.getByText(`Overall words: ${dictionary.split('\n').length}`)).toBeTruthy();
    global.fetch.mockClear();
  });
});
