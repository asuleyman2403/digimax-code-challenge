import React from 'react';

import './styles.scss';

const Search = ({ searchKey, handleChange }) => {
  return (
    <div className="Search" data-testid="search">
      <input
        data-testid="search-input"
        className="input Search__input"
        value={searchKey}
        onChange={(e) => handleChange(e.target.value)}
      />
      <button className="button Search__clear-btn" onClick={() => handleChange('')}>
        Clear
      </button>
    </div>
  );
};

export default Search;
