import React from 'react';

import './styles.scss';

const Actions = ({ isLoaded, found, addToDictionary, deleteFromDictionary }) => {
  return (
    <div className={`Actions ${isLoaded ? '--loaded' : ''}`} data-testid="actions">
      {found ? (
        <div className="Actions__add-wrapper">
          <p className="Actions__text">
            <span className="success">The word exists in dictionary.</span> Would you like to delete
            it?
          </p>
          <button className="button button--danger" data-testid="actions-delete-btn" onClick={deleteFromDictionary}>
            Delete
          </button>
        </div>
      ) : (
        <div className="Actions__add-wrapper">
          <p className="Actions__text">
            <span className="danger">
              <span className="danger">The word does not exist in dictionary.</span>
            </span>{' '}
            Would you like to add it?
          </p>
          <button className="button button--success" data-testid="actions-add-btn" onClick={addToDictionary}>
            Add
          </button>
        </div>
      )}
    </div>
  );
};

export default Actions;
