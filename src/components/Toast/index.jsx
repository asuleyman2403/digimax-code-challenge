import React from 'react';

import './styles.scss';

const Toast = ({ message }) => {
  return message ? (
    <div className={`Toast ${message.type === 'ERROR' ? '--error' : '--success'}`} data-testid="toast">
      <div className="Toast__content">{message.text}</div>
    </div>
  ) : null;
};

export default Toast;
