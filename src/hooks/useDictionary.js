import { useState, useCallback, useEffect, useMemo } from 'react';

import { LOCAL_STORAGE_KEYS, getLocalStorageItem, setLocalStorageItem } from 'utils/localStorage';

import dict from 'data/dictionary.txt';

const MESSAGE_TYPES = {
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
};

export const useDictionary = ({ messageDuration = 500 }) => {
  const [dictionary, setDictionary] = useState(new Set());
  const [key, setKey] = useState('');
  const [emittedMessage, setEmittedMessage] = useState(null);

  const emitMessage = useCallback(
    ({ text, type }) => {
      setEmittedMessage({
        text,
        type,
      });

      const timeout = setTimeout(() => {
        setEmittedMessage(null);
        clearTimeout(timeout);
      }, messageDuration);
    },
    [messageDuration],
  );

  const foundWord = useMemo(() => {
    return dictionary.has(key) && key.length ? key : null;
  }, [dictionary, key]);

  const loadDictionary = useCallback(async () => {
    try {
      const dictStr = getLocalStorageItem(LOCAL_STORAGE_KEYS.DICTIONARY);
      if (dictStr) {
        setDictionary(new Set(JSON.parse(dictStr)));
      } else {
        const readableStream = await fetch(dict);
        const text = await readableStream.text();
        setDictionary(new Set(text.split('\n')));
      }
      emitMessage({
        type: MESSAGE_TYPES.SUCCESS,
        text: 'Dictionary has been loaded!',
      });
      return Promise.resolve();
    } catch (e) {
      emitMessage({
        type: MESSAGE_TYPES.ERROR,
        text: 'Error occurred while loading!',
      });
      return Promise.reject(e);
    }
  }, [emitMessage]);

  const saveLocalDictionary = useCallback(() => {
    setDictionary((dictionary) => {
      setLocalStorageItem(LOCAL_STORAGE_KEYS.DICTIONARY, Array.from(dictionary));
      return dictionary;
    });
  }, []);

  const handleChange = useCallback((key) => {
    setKey(key);
  }, []);

  const addToDictionary = useCallback(() => {
    if (!foundWord) {
      setDictionary((dictionary) => dictionary.add(key));
      saveLocalDictionary();
      setKey('');
      emitMessage({
        type: MESSAGE_TYPES.SUCCESS,
        text: 'The word added!',
      });
    }
  }, [foundWord, key, saveLocalDictionary, emitMessage]);

  const deleteFromDictionary = useCallback(() => {
    if (foundWord) {
      setDictionary((dictionary) => {
        dictionary.delete(key);
        return dictionary;
      });
      saveLocalDictionary();
      setKey('');
      emitMessage({
        type: MESSAGE_TYPES.SUCCESS,
        text: 'The word deleted!',
      });
    }
  }, [foundWord, key, saveLocalDictionary, emitMessage]);

  useEffect(() => {
    loadDictionary();
  }, [loadDictionary]);

  return {
    size: dictionary.size,
    searchKey: key,
    found: foundWord,
    emittedMessage,
    handleChange,
    addToDictionary,
    deleteFromDictionary,
  };
};
